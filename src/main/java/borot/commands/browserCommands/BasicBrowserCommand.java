package borot.commands.browserCommands;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.base.Strings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import borot.commands.CommandBase;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;


public abstract class BasicBrowserCommand extends CommandBase{

    @Getter(value = AccessLevel.PROTECTED)
    private WebDriver driver;

    public BasicBrowserCommand(WebDriver driver) {
        super();
        this.driver = driver;
    }

    @Getter 
    @Setter
    @JacksonXmlProperty(isAttribute = true)
    private String id;

    @Getter 
    @Setter
    @JacksonXmlProperty(isAttribute = true)
    private String cssSelector;

    @Getter 
    @Setter
    @JacksonXmlProperty(isAttribute = true)
    private String name;

    @Getter 
    @Setter
    @JacksonXmlProperty(isAttribute = true)
    private String xPath;

    @Getter 
    @Setter
    @JacksonXmlProperty(isAttribute = true)
    private String tagName;

    @Getter 
    @Setter
    @JacksonXmlProperty(isAttribute = true)
    private String linkText;

    @Getter 
    @Setter
    @JacksonXmlProperty(isAttribute = true)
    private String className;

    @Getter 
    @Setter
    @JacksonXmlProperty(isAttribute = true)
    private String partialLinkText;

    protected By getLocator(){
        if(!Strings.isNullOrEmpty(id)){
            return By.id(id);
        } else if(!Strings.isNullOrEmpty(name)){
            return By.name(name);
        } else if(!Strings.isNullOrEmpty(tagName)){
            return By.tagName(tagName);
        } else if(!Strings.isNullOrEmpty(className)){
            return By.className(className);
        } else if(!Strings.isNullOrEmpty(linkText)){
            return By.linkText(linkText);
        } else if(!Strings.isNullOrEmpty(partialLinkText)){
            return By.partialLinkText(partialLinkText);
        } else if(!Strings.isNullOrEmpty(cssSelector)){
            return By.cssSelector(cssSelector);
        } else{
            return null;
        }
    }
}