package borot.commands.browserCommands;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import org.openqa.selenium.WebDriver;

import borot.commands.CommandResult;
import lombok.Getter;
import lombok.Setter;

public class NavigateCommand extends BasicBrowserCommand {
    public NavigateCommand(WebDriver driver) {
        super(driver);
    }

    @Getter
    @Setter
    @JacksonXmlProperty(isAttribute = true)
    private String url;

    @Override
    public CommandResult executionImpl() {
        getDriver().navigate().to(getUrl());
        return null;
    }

    
}