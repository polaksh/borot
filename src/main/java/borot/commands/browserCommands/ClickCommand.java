package borot.commands.browserCommands;

import org.openqa.selenium.WebDriver;

import borot.commands.CommandResult;

public class ClickCommand extends BasicBrowserCommand {

    public ClickCommand(WebDriver driver) {
        super(driver);
    }

    @Override
    public CommandResult executionImpl() {
        super.getDriver().findElement(getLocator()).click();
        return null;
    }

}