package borot.commands;

import lombok.Getter;

public abstract class CommandBase {
    
    public void execute(){
        try{
            this.result = executionImpl();
        }catch(Exception e){
            handleError(e);
        }
    }
    
    public abstract CommandResult executionImpl();
    
    public void handleError(Throwable err) {}

    @Getter
    private CommandResult result;
}