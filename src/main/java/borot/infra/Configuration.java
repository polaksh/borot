package borot.infra;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import borot.infra.configuration.elements.BrowserConfiguration;
import borot.infra.configuration.elements.RuntimeConfiguration;
import borot.infra.exceptions.HoleInTheGroundException;

public class Configuration {

    public final static String C_CONFIGURATION_ENVIRONMENT_VARIABLE = "AUTOMATION_CONFIGURATION_FILE";

    private static Object _padlock = new Object();
    private static Configuration _instance = null;

    private ObjectMapper _mapper;
    private JsonNode _root;
    private RuntimeConfiguration _runtime;
    private List<BrowserConfiguration> _browsers;

    public static Configuration instance() {
        if (_instance == null) {
            synchronized (_padlock) {
                if (_instance == null) {
                    _instance = new Configuration();
                }
            }
        }
        return _instance;
    }

    private Configuration(){
        try {
            Path path = System.getProperty(C_CONFIGURATION_ENVIRONMENT_VARIABLE, null) == null
                    ? Paths.get(getClass().getClassLoader().getResource("conf.json").toURI())
                    : Paths.get(System.getProperty(C_CONFIGURATION_ENVIRONMENT_VARIABLE));
            _mapper = new ObjectMapper();
            _root = _mapper.readTree(path.toFile());
            _mapper.readTree(_root.path("browsers").traverse());
        } catch (Exception e) {
            System.out.println(String.format("Something is wrong with the configurations: %s %s %s",e.getMessage(),System.lineSeparator(), e));
        }
    }

    public RuntimeConfiguration runtime() throws JsonParseException, JsonMappingException, IOException {
        return _mapper.readValue(_root.path("runtime").traverse(), RuntimeConfiguration.class);
    }

    public List<BrowserConfiguration> browsers() throws JsonParseException, JsonMappingException, IOException {
        return  Arrays.asList(_mapper.readValue(_root.path("browsers").traverse(), BrowserConfiguration[].class));
    }
}