package borot.infra.exceptions;

public class HoleInTheGroundException extends Exception{
    public HoleInTheGroundException() {
        super();
    }
    public HoleInTheGroundException(String message) {
        super(message);
    }
    public HoleInTheGroundException(String message, Throwable inner) {
        super(message,inner);
    }
}