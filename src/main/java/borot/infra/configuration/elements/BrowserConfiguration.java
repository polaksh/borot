package borot.infra.configuration.elements;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class BrowserConfiguration{
   
    private String name;
   
    private Map<String,String> capabilities;
   
    @JsonProperty("driver-location")
    private String driverLocation;
}