package borot.infra.configuration.elements;

import lombok.Data;

@Data
public class RuntimeConfiguration{
    private String browser;
}