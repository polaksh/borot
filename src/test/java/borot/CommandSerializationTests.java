package borot;

import java.io.IOException;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import borot.commands.browserCommands.ClickCommand;

import org.junit.jupiter.api.Assertions;

public class CommandSerializationTests {

    private XmlMapper mapper = new XmlMapper();

    @Test
    public void clickCommand() {
        String command = "<ClickCommand id=\"btnK\"/>";
        try {
            ClickCommand cmd = mapper.readValue(command, ClickCommand.class);
            Assertions.assertEquals(cmd.getId(), "btnK");
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
        }


    }
}