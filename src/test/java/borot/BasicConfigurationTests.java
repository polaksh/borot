package borot;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import borot.infra.Configuration;
import borot.infra.configuration.elements.RuntimeConfiguration;

public class BasicConfigurationTests{

    @BeforeEach
    void init(TestInfo testInfo) {
        System.out.println(">>> starting test: "+ testInfo.getDisplayName());
    }

    @Test
    @DisplayName("Runtime configuration test")
    public void runtimeConfigurationTest(){
        try{
            String browser = Configuration.instance().runtime().getBrowser();
            System.out.println("Browser is: "+browser);
        }catch(Exception e){
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("Browsers configuration test")
    public void browsersConfigurationTest(){
        try{
            List<String> browsers = Configuration.instance().browsers().stream().map(b-> b.getName()).collect(Collectors.toList());
            browsers.forEach(b-> System.out.println(b));
        }catch(Exception e){
            fail(e.getMessage());
        }
    }
}